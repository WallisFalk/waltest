package de.wallisfalk.waltest

class Failure(prefix: String) extends utest.AssertionError(prefix, Seq.empty)