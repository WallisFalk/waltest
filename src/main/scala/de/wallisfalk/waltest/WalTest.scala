package de.wallisfalk.waltest

import scala.reflect.ClassTag
import utest._

sealed trait Output
case object Result extends Output
case object Sentence extends Output

case class WalTest(output: Output, stackTrace: Boolean) {
  def checkForException(constructorOrFunction: => Any, expected: Option[Exception]): String = {
    def checkForExceptions : (Boolean, String) = {
      def sameClass(a: Any, b: Any): Boolean = {
        val B = ClassTag(b.getClass)
        ClassTag(a.getClass) match {
          case B => true
          case _ => false
        }
      }

      def tryCatchWithCreation: (Boolean, String) = {
        try {
          constructorOrFunction
          if (expected.isDefined)
            output match {
              case Sentence => (false, "No exception occurred, but expected was '" + expected.get + ".")
              case Result => (false, "No exception.")
              case _ => (false, "Output not implemented yet.")
            }
          else
            output match {
              case Sentence => (true, "No exception occurred, as expected.")
              case Result => (true, "No exception.")
              case _ => (false, "Output not implemented yet.")
            }
        }
        catch {
          case thr: Throwable =>
            if (expected.isDefined) {
              if (sameClass(thr, expected.get) && thr.getMessage == expected.get.getMessage)
                output match {
                  case Sentence => (true, "'" + thr + "' was thrown and expected.")
                  case Result => (true, "" + thr)
                  case _ => (false, "Output not implemented yet.")
                }
              else
                output match {
                  case Sentence => (false, "'" + thr + "' was thrown, but expected was '" + expected.get + "'.")
                  case Result => (false, "" + thr)
                  case _ => (false, "Output not implemented yet.")
                }
            }
            else
              output match {
                case Sentence => (false, "'" + thr + "' was thrown, but no exception was expected.")
                case Result => (false, "" + thr)
                case _ => (false, "Output not implemented yet.")
              }
        }
      }

      tryCatchWithCreation
    }

    assert(checkForExceptions)
  }

  def checkForEquality[T](function: => T, expected: T) : String = {
    def tryCatchWithResult: (Boolean, String) = {
      try {
        val result = function
        try {
          utest.assert(result == expected)
          output match {
            case Sentence => (true, "" + result + " was returned and expected.")
            case Result => (true, "" + result)
            case _ => (false, "Output not implemented yet.")
          }
        }
        catch {
          case ae: utest.AssertionError =>
            output match {
              case Sentence => (false, "'" + ae + "' was thrown, but expected was '" + expected + "'.")
              case Result => (false, "" + ae)
              case _ => (false, "Output not implemented yet.")
            }
          case other: Throwable =>
            output match {
              case Sentence => (false, "An unexpected error occurred '" + other + "'.")
              case Result => (false, "" + other)
              case _ => (false, "Output not implemented yet.")
            }
        }
      }
      catch {
        case thr: Throwable =>
          output match {
            case Sentence => (false, "'" + thr + "' was thrown, but expected was '" + expected + "'.")
            case Result => (false, "" + thr)
            case _ => (false, "Output not implemented yet.")
          }
      }
    }

    assert(tryCatchWithResult)
  }

  private def assert(result: (Boolean, String)) : String = {
    if (result._1)
      result._2
    else {
      try {
        utest.assert(result._1)
        "Unexpected error."
      }
      catch {
        case err: utest.AssertionError =>
          if (!stackTrace) {
            val fail = new Failure(err.getMessage)
            fail.setStackTrace(Array.empty)
            throw fail
          }
          else
            throw err;
        case _ => "Unexpected error."
      }
    }
  }
}

case object WalTest {
  def createExt(output: Output = Result, stackTrace: Boolean = false): WalTest = new WalTest(output, stackTrace)

  def runAndPrint(tests: Tests, label: String, exitForFailures: Boolean = true) : Unit = {
    val results = TestRunner.runAndPrint(tests, label)
    val (_, _, failures) = TestRunner.renderResults(
      Seq(
        label -> results
      )
    )
    if (exitForFailures && failures > 0)
      sys.exit(1)
  }
}
