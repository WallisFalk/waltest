package de.wallisfalk.waltest

import utest.{TestSuite, Tests}

import scala.reflect.ClassTag

case class WalTestTest(walTest: WalTest) {
  def testForExceptionTest[T](fun: => Any, expected: String): String = {
    try {
      fun match {
        case `expected` => "[SUCCESS] - " + expected
        case occurred: String => "[FAILURE] - expected was '" + expected + "' but it occurred '" + occurred + "'."
      }
    }
    catch {
      case ex: Failure => if (ex.toString == expected) "[SUCCESS] - " + ex.toString else "[FAILURE] - expected was '" + expected + "' but it occurred '" + ex.toString + "'."
    }
  }
}

case object WalTestTest extends TestSuite {
  val tests: Tests = Tests {
    val walTest = WalTest.createExt(output = Sentence)
    val walTestTest = WalTestTest(walTest)
    utest.test("exception-true--10") {
      walTestTest.testForExceptionTest(walTest.checkForException(new TestClass(-10), Some(new IllegalArgumentException("Index must be greater or equal than zero!"))), "'java.lang.IllegalArgumentException: Index must be greater or equal than zero!' was thrown and expected.")
    }
    utest.test("exception-false--10") {
      walTestTest.testForExceptionTest(walTest.checkForException(new TestClass(-10), Some(new IllegalArgumentException("Index must be smaller or equal than ZERO!"))), "de.wallisfalk.waltest.Failure: utest.assert(result._1)\nresult: (Boolean, String) = (false,'java.lang.IllegalArgumentException: Index must be greater or equal than zero!' was thrown, but expected was 'java.lang.IllegalArgumentException: Index must be smaller or equal than ZERO!'.)")
    }
    utest.test("exception-true-0") {
      walTestTest.testForExceptionTest(walTest.checkForException(new TestClass(0), None), "No exception occurred, as expected.")
    }
    utest.test("exception-true-101") {
      walTestTest.testForExceptionTest(walTest.checkForException(new TestClass(101), Some(new IllegalArgumentException("Index must be smaller or equal than 100!"))), "'java.lang.IllegalArgumentException: Index must be smaller or equal than 100!' was thrown and expected.")
    }
    utest.test("exception-false-101") {
      walTestTest.testForExceptionTest(walTest.checkForException(new TestClass(101), Some(new IllegalArgumentException("Index must be greater or equal than zero!"))), "de.wallisfalk.waltest.Failure: utest.assert(result._1)\nresult: (Boolean, String) = (false,'java.lang.IllegalArgumentException: Index must be smaller or equal than 100!' was thrown, but expected was 'java.lang.IllegalArgumentException: Index must be greater or equal than zero!'.)")
    }
    utest.test("function-true-0") {
      walTestTest.testForExceptionTest(walTest.checkForEquality(TestClass.testFunSqrt(0), 0), "0 was returned and expected.")
    }
    utest.test("function-false-0") {
      walTestTest.testForExceptionTest(walTest.checkForEquality(TestClass.testFunSqrt(0), 1), "de.wallisfalk.waltest.Failure: utest.assert(result._1)\nresult: (Boolean, String) = (false,'utest.AssertionError: utest.assert(result == expected)\nresult: T = 0\nexpected: T = 1' was thrown, but expected was '1'.)")
    }
    utest.test("function-false--1") {
      walTestTest.testForExceptionTest(walTest.checkForEquality(TestClass.testFunSqrt(-1), 0), "de.wallisfalk.waltest.Failure: utest.assert(result._1)\nresult: (Boolean, String) = (false,'java.lang.IllegalArgumentException: The square root of a negative number is not defined.' was thrown, but expected was '0'.)")
    }
  }

  def sameClass(a: Any, b: Any): Boolean = {
    val B = ClassTag(b.getClass)
    ClassTag(a.getClass) match {
      case B => true
      case _ => false
    }
  }

  def main(args: Array[String]): Unit = {
    WalTest.runAndPrint(tests, label = "WalTestTest", exitForFailures = false)
  }
}

class TestClass(index: Int) {
  if (index < 0)
    throw new IllegalArgumentException("Index must be greater or equal than zero!")
  if (index > 100)
    throw new IllegalArgumentException("Index must be smaller or equal than 100!")
}

object TestClass {
  def testFunSqrt(number: Int): Int = {
    if (number < 0)
      throw new IllegalArgumentException("The square root of a negative number is not defined.")
    Math.sqrt(number).toInt
  }
}
